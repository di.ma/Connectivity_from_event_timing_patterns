# Inferring network connectivity from event timing patterns



**Model-free method for inferring _synaptic interactions_ from _spike train recordings_.**

By mapping spike timing data in **event spaces** _(spanned by inter-spike and cross-spike intervals)_,  
we identify synaptic interactions in networks of spiking neurons through **Event Space Linearisations (ESL)** .  


Here, we provide implementations of network simulations and reconstructions as described in:  
**Casadiego\*, Jose, Maoutsa\*, Dimitra, Timme, Marc,**  
_**Inferring network connectivity from event timing patterns**_,  
**Physical Review Letters, 2018**  

For further information refer to the [article](https://gitlab.com/di.ma/Connectivity_from_event_timing_patterns/-/blob/master/PhysRevLett.121.054101.pdf) and [supplementary info](https://gitlab.com/di.ma/Connectivity_from_event_timing_patterns/-/blob/master/Supplementary_Inferring_network_connectivity_from_event_timing_patterns.pdf) . (_can be found here as pdf_).

<br>
<div align="center">
   <img src="event_space.png" alt="mapping events to event space by dimitra maoutsa" width="60%" height="60%">
   
   </div>

<br>

### Running the code:
1. Generate input data
    - Either extract provided data
    
        ```
        cd Connectivity_from_event_timing_patterns/simulate_network
        tar -xzvf Data.tar.gz
        ```
    - Or simulate network (requires [NEST simulator] (http://www.nest-simulator.org/) )
    
        ```
        python Connectivity_from_event_timing_patterns/simulate_network/simulate_network.py
        ```
2. Reconstruct
    ```bash
    python Connectivity_from_event_timing_patterns/reconstruct_network/inferring_connections_from_spikes.py
    
    ```
    **Caution:** Input data files should be stored in folder `simulate_network/Data/`



<br>

### Support:
For questions please contact: Dimitra Maoutsa [ dimitra.maoutsa <-at-> tu-berlin.de ] 

### Cite:
```
@article{ESL18,
  title = {Inferring Network Connectivity from Event Timing Patterns},
  author = {Casadiego, Jose and Maoutsa, Dimitra and Timme, Marc},
  journal = {Phys. Rev. Lett.},
  volume = {121},
  issue = {5},
  pages = {054101},
  numpages = {6},
  year = {2018},
  month = {Aug},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevLett.121.054101},
  url = {https://link.aps.org/doi/10.1103/PhysRevLett.121.054101}
}

```
